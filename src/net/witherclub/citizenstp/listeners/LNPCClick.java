package net.witherclub.citizenstp.listeners;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import net.citizensnpcs.api.event.NPCClickEvent;
import net.witherclub.citizenstp.CitizensTP;

public class LNPCClick implements Listener {

	@EventHandler
	public boolean npcClickEvent( NPCClickEvent event ) {
		
		CitizensTP.logger.info("NPC Clicked On....");
		
		return false;
	}
}
