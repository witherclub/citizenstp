package net.witherclub.citizenstp;

import java.util.logging.Logger;

import org.bukkit.plugin.java.JavaPlugin;

import net.witherclub.citizenstp.listeners.LCommand;
import net.witherclub.citizenstp.listeners.LNPCClick;

public class CitizensTP extends JavaPlugin {
	
	public final static Logger logger = Logger.getLogger("Minecraft");
	
	public void onEnable() {
		
		this.getCommand("citizenstp").setExecutor(new LCommand());
		
		getServer().getPluginManager().registerEvents(new LNPCClick(), this);
	}
	
	public void onDisable() {
		
	}
}
